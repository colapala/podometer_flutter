import 'package:flutter/material.dart';
import 'dart:async';

import 'package:pedometer/pedometer.dart';
import 'package:shared_preferences/shared_preferences.dart';

String formatDate(DateTime d) {
  return d.toString().substring(0, 19);
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Stream<StepCount> _stepCountStream;
  String _steps = '?';
  String _oldRecord = '?';
  String _oldDate = '?';
  int stepsBeforeReboot = 0;
  String _beforeReboot = '?';
  SharedPreferences _prefs;

  int stepsByDay = 0;
  String _stepsDays = "?";

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  void onStepCount(StepCount event) async {
    int steps = event.steps;
    DateTime timeStamp = event.timeStamp;
    print('steps : ' + steps.toString() + ' at ' + timeStamp.toString());

    int lastRecordedStepsCount = _prefs.getInt('lastRecordedStepsCount') ?? -1;
    String lastRecordedStepsDate =
        _prefs.getString('lastRecordedStepsDate') ?? "";
    int currentRecordedStepsCount =
        _prefs.getInt('currentRecordedStepsCount') ?? 0;
    String currentRecordedStepsDate =
        _prefs.getString('currentRecordedStepsDate') ?? "";
    DateTime currentDate = DateTime.now();

    if (lastRecordedStepsCount == -1) {
      // The last record steps count has not been set
      // Set the var
      print('update old record');
      lastRecordedStepsCount = steps;
      await _prefs.setInt('lastRecordedStepsCount', lastRecordedStepsCount);
      print('update date');
      lastRecordedStepsDate = event.timeStamp.toString();
      await _prefs.setString('lastRecordedStepsDate', lastRecordedStepsDate);
    }

    // If the lastRecordedStepsCount is more than the actual steps count 
    // -> the phone has been reboot ( true while lastRecordedStepsCount = 0 )
    if (lastRecordedStepsCount > steps) {
      // The device has been rebooted, we must reset lastRecordedStepsCount
      print('reset old record to 0');
      lastRecordedStepsCount = 0;
      await _prefs.setInt('lastRecordedStepsCount', lastRecordedStepsCount);
      print('reset date');
      lastRecordedStepsDate = event.timeStamp.toString();
      await _prefs.setString('lastRecordedStepsDate', lastRecordedStepsDate);
      DateTime dateBeforeReboot = DateTime.parse(currentRecordedStepsDate);
      // If the dateBeforeReboot is the same as the currentDate, 
      // we must take the currentRecordedStepsCount into account in the steps count
      // Else, we must set the steps count for the dateBeforeReboot
      if (currentDate.year == dateBeforeReboot.year &&
              currentDate.month == dateBeforeReboot.month &&
              currentDate.day == dateBeforeReboot.day &&
              currentDate.hour ==
                  dateBeforeReboot
                      .hour /*&&
        currentDate.minute == recordedDate.minute*/
          ) {
        stepsBeforeReboot = currentRecordedStepsCount;
      } else {
        // The last activity has not been saved yet, save it
        print('save data');
        stepsByDay = currentRecordedStepsCount;
        stepsBeforeReboot = 0;
        // Save currentRecordStepsCount
      }
    }

    // Update current record
    currentRecordedStepsCount =
        steps - lastRecordedStepsCount + stepsBeforeReboot;
    await _prefs.setInt('currentRecordedStepsCount', currentRecordedStepsCount);
    currentRecordedStepsDate = event.timeStamp.toString();
    await _prefs.setString(
        'currentRecordedStepsDate', currentRecordedStepsDate);

    DateTime recordedDate = DateTime.parse(lastRecordedStepsDate);

    // If the actual Day is different from the last recorded day,
    // Then, save the currentRecordedStepsCount
    // Reset the lastRecordedStepsCount to the actual number of steps
    if (!(currentDate.year == recordedDate.year &&
            currentDate.month == recordedDate.month &&
            currentDate.day == recordedDate.day &&
            currentDate.hour ==
                recordedDate
                    .hour /*&&
        currentDate.minute == recordedDate.minute*/
        )) {
      print('steps by day : ' + currentRecordedStepsCount.toString());
      print('save the data');
      stepsByDay = currentRecordedStepsCount;
      lastRecordedStepsCount = steps;
      await _prefs.setInt('lastRecordedStepsCount', lastRecordedStepsCount);
      lastRecordedStepsDate = event.timeStamp.toString();
      await _prefs.setString('lastRecordedStepsDate', lastRecordedStepsDate);

      // Reset the steps before reboot
      stepsBeforeReboot = 0;
    }

    setState(() {
      _steps = currentRecordedStepsCount.toString();
      _oldRecord = lastRecordedStepsCount.toString();
      _oldDate = lastRecordedStepsDate.toString();
      _beforeReboot = stepsBeforeReboot.toString();
      _stepsDays = stepsByDay.toString();
    });
  }

  void onStepCountError(error) {
    print('onStepCountError: $error');
    setState(() {
      _steps = 'Step Count not available';
    });
  }

  void initPlatformState() async {
    _stepCountStream = Pedometer.stepCountStream;
    _stepCountStream.listen(onStepCount).onError(onStepCountError);

    _prefs = await SharedPreferences.getInstance();

    if (!mounted) return;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Pedometer example app'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Steps taken:',
                style: TextStyle(fontSize: 30),
              ),
              Text(
                _steps,
                style: TextStyle(fontSize: 60),
              ),
              Divider(
                height: 30,
                thickness: 0,
                color: Colors.white,
              ),
              Text(
                'Old steps record:',
                style: TextStyle(fontSize: 30),
              ),
              Text(
                _oldRecord,
                style: TextStyle(fontSize: 20),
              ),
              Divider(
                height: 30,
                thickness: 0,
                color: Colors.white,
              ),
              Text(
                'Old record Date:',
                style: TextStyle(fontSize: 30),
              ),
              Text(
                _oldDate,
                style: TextStyle(fontSize: 20),
              ),
              Divider(
                height: 50,
                thickness: 0,
                color: Colors.white,
              ),
              Text(
                'Before Reboot:',
                style: TextStyle(fontSize: 30),
              ),
              Text(
                _beforeReboot,
                style: TextStyle(fontSize: 20),
              ),
              Divider(
                height: 30,
                thickness: 0,
                color: Colors.white,
              ),
              Text(
                'nbSteps by day:',
                style: TextStyle(fontSize: 30),
              ),
              Text(
                _stepsDays,
                style: TextStyle(fontSize: 20),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
